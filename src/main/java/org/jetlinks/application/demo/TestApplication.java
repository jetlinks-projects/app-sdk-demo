package org.jetlinks.application.demo;

import org.jetlinks.application.sdk.ApplicationContext;
import org.jetlinks.application.sdk.ApplicationEndpoint;
import org.jetlinks.application.sdk.ApplicationModuleService;

public class TestApplication implements ApplicationEndpoint {

    @Override
    public void start(ApplicationContext applicationContext) {
        ApplicationModuleService<TestData> data = applicationContext.getModuleService(TestData.class);

        //
    }

    @Override
    public void dispose() {

    }
}
