package org.jetlinks.application.demo;

import org.jetlinks.application.sdk.ApplicationData;
import org.jetlinks.application.sdk.annotation.Module;

@Module("test-module")
public class TestData extends ApplicationData {
}
